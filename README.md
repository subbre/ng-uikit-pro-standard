
# NgUikitProStandard

# Modules list

MDB Angular Free:

* ButtonsModule,
* CarouselModule,
* ChartsModule,
* CollapseModule,
* InputsModule,
* ModalModule,
* NavbarModule,
* PopoverModule,
* TooltipModule,
* WavesModule,
* MDBBootstrapModule - contains every MDB Angular Free modules.

MDB Angular Pro:

* AccordionModule,
* ToastModule,
* AutocompleteModule,
* CardsModule,
* DatepickerModule,
* ChartSimpleModule,
* FileInputModule,
* CharCounterModule,
* LightBoxModule,
* SelectModule,
* PreloadersModule,
* SidenavModule,
* SmoothscrollModule,
* StickyContentModule,
* TabsModule,
* MaterialChipsModule,
* TimePickerModule,
* MDBBootstrapModulePro - contains every MDB Angular Pro modules,
* MDBBootstrapModulesPro - contains MDBBootstrapModule and MDBBootstrapModulePro modules.
